package com.mimacom.furiousfurriels;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Problem {

    public List<Contributor> contributors = new ArrayList<>();
    public List<Project> projects = new ArrayList<>();

    public Set<String> uniqueSkills = new HashSet<>();
}
