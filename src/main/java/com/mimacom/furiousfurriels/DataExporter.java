package com.mimacom.furiousfurriels;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class DataExporter {

    public static void saveResult(Result result, String file) throws Exception {
        List<String> lines = new ArrayList<>();
        lines.add(result.projects.size() + "");
        result.projects.forEach(project -> {
            lines.add(project.name);
            List<String> sortedContributors = new ArrayList<>();
            for(String skill : project.sortedRoles) {
                sortedContributors.add(project.contributors.get(skill).name);
            }
            lines.add(sortedContributors.stream().collect(Collectors.joining(" ")));
        });

        Files.write(Path.of(file), lines);
    }
}
