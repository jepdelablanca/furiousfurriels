package com.mimacom.furiousfurriels;

import java.io.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DataImporter {

    static Pattern HEADER = Pattern.compile("(?<contributors>\\d+)\\s(?<projects>\\d+)");
    static Pattern CONTRIBUTOR = Pattern.compile("(?<name>[^\\s]+)\\s(?<skills>\\d+)");
    static Pattern SKILL = Pattern.compile("(?<skill>[^\\s]+)\\s(?<level>\\d+)");

    static Pattern PROJECT = Pattern.compile("(?<name>[^\\s]+)\\s(?<duration>\\d+)\\s(?<completionScore>\\d+)\\s(?<bestBefore>\\d+)\\s(?<roles>\\d+)");

    public static Problem loadProblem(String file) throws Exception {
        //InputStream is = DataImporter.class.getResourceAsStream(file);
        try (BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(new File(file))))) {
            Problem result = new Problem();
            Matcher headerLine = HEADER.matcher(in.readLine());
            headerLine.matches();
            int contributors = Integer.parseInt(headerLine.group("contributors"));
            int projects = Integer.parseInt(headerLine.group("projects"));
            for(int c=0; c<contributors; c++) {
                Matcher contributorLine = CONTRIBUTOR.matcher(in.readLine());
                contributorLine.matches();
                Contributor contributor = new Contributor(contributorLine.group("name"));
                int skills = Integer.parseInt(contributorLine.group("skills"));
                for(int d=0; d<skills; d++) {
                    Matcher skillLine = SKILL.matcher(in.readLine());
                    skillLine.matches();
                    String skill = skillLine.group("skill");
                    result.uniqueSkills.add(skill);
                    int level = Integer.parseInt(skillLine.group("level"));
                    contributor.skills.put(skill, level);
                }
                result.contributors.add(contributor);
            }
            for(int c=0; c<projects; c++) {
                Matcher projectLine = PROJECT.matcher(in.readLine());
                projectLine.matches();
                Project project = new Project(projectLine.group("name"));
                project.duration = Integer.parseInt(projectLine.group("duration"));
                project.completionScore = Integer.parseInt(projectLine.group("completionScore"));
                project.bestBefore = Integer.parseInt(projectLine.group("bestBefore"));
                int roles = Integer.parseInt(projectLine.group("roles"));
                for(int d=0; d<roles; d++) {
                    // Copypasteeee
                    Matcher skillLine = SKILL.matcher(in.readLine());
                    skillLine.matches();
                    String skill = skillLine.group("skill");
                    project.sortedRoles.add(skill);
                    result.uniqueSkills.add(skill);
                    int level = Integer.parseInt(skillLine.group("level"));
                    project.roles.put(skill, level);
                }
                result.projects.add(project);
            }
            return result;
        } catch(Exception e) {
            // Just wrapping
            throw new Exception("Error loading problem: "+ (e==null? "null": e.getMessage()), e);
        }
    }
}
