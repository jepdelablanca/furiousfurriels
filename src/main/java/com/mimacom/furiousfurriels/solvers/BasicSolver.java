package com.mimacom.furiousfurriels.solvers;

import com.mimacom.furiousfurriels.Contributor;
import com.mimacom.furiousfurriels.Problem;
import com.mimacom.furiousfurriels.Project;
import com.mimacom.furiousfurriels.Result;

import java.lang.reflect.Array;
import java.util.*;

public class BasicSolver {

    public Result solve(Problem problem) {
        Result result = new Result();
        LinkedList<Project> queue = new LinkedList<>();
        problem.projects.forEach(project -> queue.add(project));

        while(!queue.isEmpty()) {
            Project project = queue.pop();

            List<Contributor> unassignedContributors = new ArrayList(problem.contributors);

            for(String role: project.roles.keySet()) {
                int level = project.roles.get(role);
                Optional<Contributor> contributor = findContributor(unassignedContributors, role, level);
                if(contributor.isPresent()) {
                    project.contributors.put(role, contributor.get());
                } else {
                    project.contributors.clear();
                    queue.add(project);
                    break;
                }
            }
            if(!project.contributors.isEmpty()) {
                levelUpProjectContributors(project);
                result.projects.add(project);
            }
        }
        return result;
    }

    private Optional<Contributor> findContributor(List<Contributor> contributors, String skill, int requiredLevel) {
        return contributors.stream()
                .filter(contributor->contributor.skills.keySet().contains(skill) && contributor.skills.get(skill) >= requiredLevel).findAny();
    }

    private void levelUpProjectContributors(Project project) {
        for(String role : project.contributors.keySet()) {
            Contributor contributor = project.contributors.get(role);
            int requiredLevel = project.roles.get(role);
            int currentLevel = contributor.skills.get(role);
            if(currentLevel == requiredLevel) {
                contributor.skills.put(role, ++currentLevel);
            }
        }
    }
}
