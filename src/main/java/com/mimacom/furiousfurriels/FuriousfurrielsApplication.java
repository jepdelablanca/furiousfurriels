package com.mimacom.furiousfurriels;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FuriousfurrielsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FuriousfurrielsApplication.class, args);
	}
}
