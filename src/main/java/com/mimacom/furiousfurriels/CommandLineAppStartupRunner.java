package com.mimacom.furiousfurriels;

import com.mimacom.furiousfurriels.solvers.BasicSolver;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@Component
public class CommandLineAppStartupRunner implements CommandLineRunner {

    @Override
    public void run(String...args) throws Exception {
        Problem problem = DataImporter.loadProblem("/Users/jorge/sources/input_files/a_an_example.in.txt");
        Result result = new BasicSolver().solve(problem);
        DataExporter.saveResult(result, "a_an_example.out.txt");
    }
}
