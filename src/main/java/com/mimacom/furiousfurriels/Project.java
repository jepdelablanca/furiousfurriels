package com.mimacom.furiousfurriels;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Project {

    public String name;
    public int duration;
    public int completionScore;
    public int bestBefore;
    public Map<String, Integer> roles = new HashMap<>();

    public List<String> sortedRoles = new ArrayList<String>();

    // Para la salida:
    public Map<String, Contributor> contributors = new HashMap<>();


    public Project(String name) {
        this.name = name;
    }
}
