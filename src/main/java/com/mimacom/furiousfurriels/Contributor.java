package com.mimacom.furiousfurriels;

import java.util.HashMap;
import java.util.Map;

public class Contributor {

    public String name;
    public Map<String, Integer> skills = new HashMap<>();

    public Contributor(String name) {
        this.name = name;
    }
}
